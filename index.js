module.exports = {

  Assertion: require('./src/Assertion'),
  DeepEqualAssertion: require('./src/DeepEqualAssertion'),
  DeepStrictEqualAssertion: require('./src/DeepStrictEqualAssertion'),
  EqualAssertion: require('./src/EqualAssertion'),
  FailedAssertion: require('./src/FailedAssertion'),
  FailedAssertionWithMessageOnly: require('./src/FailedAssertionWithMessageOnly'),
  IfErrorAssertion: require('./src/IfErrorAssertion'),
  NotDeepEqualAssertion: require('./src/NotDeepEqualAssertion'),
  NotDeepStrictEqualAssertion: require('./src/NotDeepStrictEqualAssertion'),
  NotEqualAssertion: require('./src/NotEqualAssertion'),
  NotStrictEqualAssertion: require('./src/NotStrictEqualAssertion'),
  NotThrownErrorAssertion: require('./src/NotThrownErrorAssertion'),
  OkAssertion: require('./src/OkAssertion'),
  StrictEqualAssertion: require('./src/StrictEqualAssertion'),
  ThrownErrorAssertion: require('./src/ThrownErrorAssertion')

}
